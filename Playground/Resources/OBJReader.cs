﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

using OpenTK;

using Playground.Datatypes;
using Playground.Graphics;

namespace Playground.Resources.OBJ
{
    static class OBJReader
    {
        //Read an object file and return a model3D
        public static Model3D ReadOBJ(String filename)
        {
            string filePath = "Resources\\Models\\" + filename;

            //Check if the file exists
            if (!File.Exists(filePath))
            {
                LOG.e("Trying to read non-existing file " + filePath);
                return null;
            }

            //Read all lines from the file
            String[] lines = File.ReadAllLines(filePath);

            //Set up storage for the different lines
            List<Vector3> v = new List<Vector3>(); //vertices
            List<Vector3> vn = new List<Vector3>(); //normals
            List<Vector2> vt = new List<Vector2>(); //texture coordinates
            List<int[][]> f = new List<int[][]>(); //faces

            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i];

                //skip comments
                if (line.Substring(0, 1) == "#") continue;

                //Split the string
                String[] splitLine = line.Split(' ');

                //check the type of line
                if (splitLine[0] == "v")
                {
                    v.Add(new Vector3(float.Parse(splitLine[1], CultureInfo.InvariantCulture), float.Parse(splitLine[2], CultureInfo.InvariantCulture), -float.Parse(splitLine[3], CultureInfo.InvariantCulture)));
                }
                else if (splitLine[0] == "vt")
                {
                    vt.Add(new Vector2(float.Parse(splitLine[1], CultureInfo.InvariantCulture), float.Parse(splitLine[2], CultureInfo.InvariantCulture)));
                }
                else if (splitLine[0] == "vn")
                {
                    vn.Add(new Vector3(float.Parse(splitLine[1], CultureInfo.InvariantCulture), float.Parse(splitLine[2], CultureInfo.InvariantCulture), float.Parse(splitLine[3], CultureInfo.InvariantCulture)));
                }
                else if (splitLine[0] == "f")
                {
                    int[][] face = new int[3][];
                    for (int j = 0; j < 3; j++)
                    {
                        String[] splitFace = splitLine[1 + j].Split('/');
                        face[j] = new int[3];
                        face[j][0] = int.Parse(splitFace[0]) - 1;
                        face[j][1] = int.Parse(splitFace[1]) - 1;
                        face[j][2] = int.Parse(splitFace[2]) - 1;
                    }
                    f.Add(face);
                }
            }

            AnimVertex[] vertices = new AnimVertex[f.Count * 3];
            int[] elements = new int[f.Count * 3];

            for (int i = 0; i < f.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Vector3 vert = v[f[i][j][0]];
                    float weight = vert.Z * 0.2f + vert.Xy.Length * 0.6f;
                    Vector4 origin = new Vector4(0, 0, weight - vert.Z, 0);
                    vertices[i * 3 + j] = new AnimVertex(vert, vn[f[i][j][2]], vt[f[i][j][1]], origin, new Vector4(weight));
                    elements[i * 3 + j] = i * 3 + j;
                }
            }

            return AnimModel3D.FromVertsFaces(vertices, elements, -1);
        }
    }
}
