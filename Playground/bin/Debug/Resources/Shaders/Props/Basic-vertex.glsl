#version 330
 
uniform vec3 light = vec3(10,2,8);

//The view-projection matrix of the scene
uniform mat4 viewProjectionMatrix;
//The model matrix for this model
uniform mat4 modelMatrix;

out float intensity;
 
void main()
{	
	gl_Position = viewProjectionMatrix * modelMatrix * gl_Vertex;
	intensity = gl_Vertex.x + gl_Vertex.z + gl_Vertex.y;
}