#version 330

out vec4 fragColor;

in float intensity;
 
void main()
{	
	fragColor = vec4(intensity*4, 0.0, 1.0, 1.0);
}