#version 330
 
uniform vec3 light = vec3(10,2,8);

//The view-projection matrix of the scene
uniform mat4 viewProjectionMatrix;
//The model matrix for this model
uniform mat4 modelMatrix;

out float intensity;
 
void main()
{	
	vec4 vert = gl_Vertex;
	vert.z = (sin(vert.x*0.6) + sin(vert.y))/6.0;
	gl_Position = viewProjectionMatrix * modelMatrix * vert;
	intensity = (vert.z+0.6);
	gl_TexCoord[0] = gl_MultiTexCoord0;
}