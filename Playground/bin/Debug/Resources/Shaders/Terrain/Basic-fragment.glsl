#version 330

uniform sampler2D colorTex;

out vec4 fragColor;

in float intensity;
 
void main()
{	
	vec2 texCoord = gl_TexCoord[0].st;
	vec4 color = texture2D(colorTex, texCoord);
	fragColor = color;
}