﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

namespace Playground.Datatypes
{
    class AxisAngle
    {
        public Vector3 Axis;
        public float Angle;

        public AxisAngle()
        {
            this.Axis = new Vector3(1, 0, 0);
            this.Angle = 0;
        }

        public AxisAngle(float x, float y, float z, float a)
        {
            this.Axis = new Vector3(x, y, z);
            this.Angle = a;
        }

        public AxisAngle(Vector3 ax, float a)
        {
            this.Axis = ax;
            this.Angle = a;
        }

        public Matrix4 ToMatrix()
        {
            float c = (float)Math.Cos(this.Angle);
            float s = (float)Math.Sin(this.Angle);
            float t = 1 - c;
            return new Matrix4(
                t * this.Axis.X * this.Axis.X + c, t * this.Axis.X * this.Axis.Y - this.Axis.Z * s, t * this.Axis.X * this.Axis.Z + this.Axis.Y * s, 0,
                t * this.Axis.X * this.Axis.Y + this.Axis.Z * s, t * this.Axis.Y * this.Axis.Y + c, t * this.Axis.Y * this.Axis.Z - this.Axis.X * s, 0,
                t * this.Axis.X * this.Axis.Z - this.Axis.Y * s, t * this.Axis.Y * this.Axis.Z + this.Axis.X * s, t * this.Axis.Z * this.Axis.Z + c, 0,
                0, 0, 0, 1
                );
        }
    }
}
