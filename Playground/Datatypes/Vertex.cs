﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using OpenTK;

namespace Playground.Datatypes
{
    [StructLayout(LayoutKind.Sequential)]
    struct Vertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoord;

        //Set constants for drawing
        public const int Size = sizeof(float) * 8;
        public const int VertexOffset = 0;
        public const int NormalOffset = sizeof(float)*3;
        public const int TexCoordOffset = sizeof(float) * 6;

        public Vertex(Vector3 p, Vector3 n, Vector2 t)
        {
            this.Position = p;
            this.Normal = n;
            this.TexCoord = t;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    struct AnimVertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoord;
        public Vector4 Indices;
        public Vector4 Weights;

        //Set constants for drawing
        public const int Size = sizeof(float) * 16;
        public const int VertexOffset = 0;
        public const int NormalOffset = sizeof(float) * 3;
        public const int TexCoordOffset = sizeof(float) * 6;
        public const int IndicesOffset = sizeof(float) * 8;
        public const int WeightOffset = sizeof(float) * 12;

        public AnimVertex(Vector3 p, Vector3 n, Vector2 t, Vector4 i, Vector4 w)
        {
            this.Position = p;
            this.Normal = n;
            this.TexCoord = t;
            this.Indices = i;
            this.Weights = w;
        }
    }
}
