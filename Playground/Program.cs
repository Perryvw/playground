﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

using Playground.Core;

namespace Playground
{
    class MyApplication
    {
        [STAThread]
        public static void Main()
        {
            //MAIN ENTRY POINT OF THE PROGRAM
            using (var game = new GameWindow(1280, 720))
            {
                //Initialise the game instance
                Game.Init(game);

                //Do this upon loading
                game.Load += (sender, e) =>
                {
                    Game.OnLoad();
                };

                //Do this when the window is resized
                game.Resize += (sender, e) =>
                {
                    Game.OnResize(game.Width, game.Height);
                };

                //Update the game state
                game.UpdateFrame += (sender, e) =>
                {
                    Game.Update();
                };

                //Render the game
                game.RenderFrame += (sender, e) =>
                {
                    Game.Render();

                    game.SwapBuffers();
                };

                // Run the game at 60 updates per second
                game.Run(60.0);
            }
        }
    }
}