﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

namespace Playground.World
{
    class GameEnvironment
    {
        public Vector3 Wind;
        public Vector3 WindDirection = new Vector3(1, 0, 0);
        public float WindForce = 9;
        public bool WindEnabled = false;

        public GameEnvironment()
        {
            this.Wind = new Vector3();
        }

        public void Update()
        {
            //Check if there is any wind
            if (WindEnabled && WindForce > 0)
            {
                //There is wind, figure out the deviation at the current time
                float delta = (float)Math.Sin(Environment.TickCount * Math.PI / (20000f/WindForce)) + (float)Math.Sin(Environment.TickCount * Math.PI / (8000f/WindForce)) + 2f;
                float constant = (WindForce - 1) * 0.08f;
                delta = 2f * constant + delta * (1-constant);

                //Multiply the deviation by the magnitude (windForce)
                this.Wind = WindDirection * (delta * (WindForce*WindForce / 100f));
            }
            else
            {
                //No wind -> 0,0,0 wind vector
                this.Wind = new Vector3();
            }
        }
    }
}
