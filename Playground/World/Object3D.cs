﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Playground.Datatypes;
using Playground.Graphics.Managers;

namespace Playground.Graphics
{
    /*
     * Highest level 3D object containing a position rotation and scale.
     */
    class Object3D
    {
        public Vector3 Position;
        public AxisAngle Rotation; //Axis angle in the form XYZA
        public Vector3 Scale;

        public Model3D Model;

        public Object3D()
        {
            this.Position = new Vector3();
            this.Rotation = new AxisAngle();
            this.Scale = new Vector3(1);
        }

        public Object3D(Vector3 pos, AxisAngle rot, Vector3 scale)
        {
            this.Position = pos;
            this.Rotation = rot;
            this.Scale = scale;
        }

        public Object3D(Model3D model) : this() {
            this.Model = model;
        }

        public void SetTexture(String texName)
        {
            this.Model.SetTexture(texName);
        }

        //Draw function to be overriden by subclasses
        public void Draw()
        {
            //Send the model matrix to the shader
            Matrix4 modelMatrix = MakeModelMatrix();
            ShaderManager.SetUniform("modelMatrix", modelMatrix);
            
            //Draw the model if it exists, otherwise draw a small cube
            if (Model != null) {
                Model.Draw();
            } else {
                Model3D.DrawCube(1f);
            }
        }

        //Make a model matrix
        protected Matrix4 MakeModelMatrix()
        {
            Matrix4 positionMatrix = Matrix4.Identity;
            positionMatrix.Row3.Xyz = this.Position;

            Matrix4 rotationMatrix = Matrix4.CreateFromAxisAngle(this.Rotation.Axis, this.Rotation.Angle);

            Matrix4 scaleMatrix = Matrix4.Identity;
            scaleMatrix.Diagonal = new Vector4(this.Scale, 1);
            return scaleMatrix * rotationMatrix * positionMatrix;
        }
    }
}
