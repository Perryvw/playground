﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

using Playground.Datatypes;
using Playground.Core;
using Playground.Graphics;

namespace Playground.World
{
    static class GameWorld
    {
        public static Camera3D Camera;
        public static GameEnvironment Environment;
        public static List<Object3D> Props;
        public static List<Object3D> Nature;
        public static List<Object3D> WaterBodies;
        public static List<Light> Lights;
        public static GameTerrain Terrain;

        //Initialise the game world
        public static void Init()
        {
            //Initialise lists
            Props = new List<Object3D>();
            Nature = new List<Object3D>();
            WaterBodies = new List<Object3D>();

            //Set up the camera
            Camera = new Camera3D();
            Camera.Position = new Vector3(-1,-1,1);
            Camera.Target = new Vector3(0, 0, 0.7f);

            //Initialise the environment
            Environment = new GameEnvironment();

            //Make some terrain
            Terrain = new GameTerrain();

            //Initialise the tree
            Object3D trunk = new Object3D(Playground.Resources.OBJ.OBJReader.ReadOBJ("trunk.obj"));
            trunk.SetTexture("bark.dds");
            trunk.Scale = new Vector3(0.6f);
            trunk.Position = new Vector3(0, 0, 0.1f);
            Nature.Add(trunk);

            Object3D leaves = new Object3D(Playground.Resources.OBJ.OBJReader.ReadOBJ("leaves.obj"));
            leaves.SetTexture("leaves.dds");
            leaves.Scale = new Vector3(0.6f);
            leaves.Position = new Vector3(0, 0, 0.1f);
            Nature.Add(leaves);

            //Add some water to initialise
            Object3D water = new Object3D();
            water.Scale= new Vector3(4,4,0.5f);
            water.Position = new Vector3(0,0,-0.5f);
            WaterBodies.Add(water);
        }
        
        //Update the game world
        public static void Update()
        {
            HandleInput();

            //Update the game environment
            Environment.Update();
        }

        //Draw the terrain
        public static void DrawTerrain()
        {

        }

        //Draw all nature objects
        public static void DrawNature()
        {
            foreach (Object3D obj in Nature)
            {
                obj.Draw();
            }
        }

        //Draw the game world geometry
        public static void DrawProps()
        {
            foreach (Object3D prop in Props)
            {
                prop.Draw();
            }
        }

        //Draw all bodies of water
        public static void DrawWater()
        {
            foreach (Object3D prop in WaterBodies)
            {
                prop.Draw();
            }
        }

        //============PRIVATE========================

        //Handle any input
        private static void HandleInput()
        {
            //Exit on alt-F4
            if (InputHandler.KeyState[Key.AltLeft] && InputHandler.KeyState[Key.F4])
            {
                LOG.v("Exiting application...");
                Game.Exit();
            }

            if (InputHandler.KeyState[Key.W]) Camera.Position.Y += 0.01f;
            if (InputHandler.KeyState[Key.S]) Camera.Position.Y -= 0.01f;
            if (InputHandler.KeyState[Key.A]) Camera.Position.X -= 0.01f;
            if (InputHandler.KeyState[Key.D]) Camera.Position.X += 0.01f;
            if (InputHandler.KeyState[Key.Z]) Camera.Position.Z -= 0.01f;
            if (InputHandler.KeyState[Key.Q]) Camera.Position.Z += 0.01f;
            if (InputHandler.KeyState[Key.K]) Environment.WindEnabled = true;
            if (InputHandler.KeyState[Key.U]) Environment.WindForce = 2;
            if (InputHandler.KeyState[Key.I]) Environment.WindForce = 5;
            if (InputHandler.KeyState[Key.O]) Environment.WindForce = 9;

        }
    }
}
