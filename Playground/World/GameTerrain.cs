﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

using Playground.Graphics;
using Playground.Graphics.Managers;
using Playground.Datatypes;

namespace Playground.World
{
    class GameTerrain
    {
        private Object3D Mesh;

        public GameTerrain()
        {
            //Generate some terrain for now
            Vertex[] vertices = new Vertex[256];
            int[] faces =  new int[1446];

            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    vertices[j + i * 16] = new Vertex(new Vector3(-2 + j * 0.25f, -2 + i * 0.25f, 0), new Vector3(0, 0, 1), new Vector2(1 - j / 15f, 1 - i / 15f));

                    int face = j * 2 + i*30;
                    faces[face * 3] = j + i * 16;
                    faces[face * 3 + 1] = j + (i+1) * 16;
                    faces[face * 3 + 2] = j+1 + i * 16;

                    faces[face * 3 + 3] = j + (i + 1) * 16;
                    faces[face * 3 + 4] = j+1 + (i + 1) * 16;
                    faces[face * 3 + 5] = j + 1 + i * 16;
                }
            }

            int texture = TextureManager.loadTexture("grass.dds");
            this.Mesh = new Object3D(Model3D.FromVertsFaces(vertices, faces, texture));
        }
        
        public void Draw()
        {
            Mesh.Draw();
        }
    }
}
