﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Playground.Graphics.Managers
{    
    /*
     * The shader manager that control the loading and reading of different shaders.
     */
    static class ShaderManager
    {
        public static Dictionary<string, int> shaderPrograms;
        private static int boundProgram = -1;

        //Initialise
        public static void Init()
        {
            shaderPrograms = new Dictionary<string, int>();
        }

        //Initialise a shader program by name or return the program if it already exists.
        public static int MakeProgram(string name)
        {
            //Check if the shader already exists
            if (shaderPrograms.ContainsKey(name))
            {
                return shaderPrograms[name];
            }
            else
            {
                //Create a new shader program
                int shaderProgram = GL.CreateProgram();

                int vertexShaderHandle = GL.CreateShader(ShaderType.VertexShader);
                int fragmentShaderHandle = GL.CreateShader(ShaderType.FragmentShader);

                //Read the shader source
                String vertexShaderSource = System.IO.File.ReadAllText("Resources\\Shaders\\" + name.Replace('.','\\') + "-vertex.glsl");
                String fragmentShaderSource = System.IO.File.ReadAllText("Resources\\Shaders\\" + name.Replace('.', '\\') + "-fragment.glsl");

                //Compile the shader
                GL.ShaderSource(vertexShaderHandle, vertexShaderSource);
                GL.ShaderSource(fragmentShaderHandle, fragmentShaderSource);

                GL.CompileShader(vertexShaderHandle);
                GL.CompileShader(fragmentShaderHandle);

                LOG.d("Initialising shader: "+name);
                String vertexLog = GL.GetShaderInfoLog(vertexShaderHandle);
                String fragmentLog = GL.GetShaderInfoLog(fragmentShaderHandle);

                //Output logs if they exist
                if (vertexLog.Length > 0) LOG.e(vertexLog);
                if (fragmentLog.Length > 0) LOG.e(fragmentLog);

                //Attach the shader to the shader program
                GL.AttachShader(shaderProgram, vertexShaderHandle);
                GL.AttachShader(shaderProgram, fragmentShaderHandle);

                GL.LinkProgram(shaderProgram);

                shaderPrograms[name] = shaderProgram;

                return shaderProgram;
            }
        }

        //Bind a shader program and return the 
        public static int BindProgram(string name)
        {
            if (shaderPrograms.ContainsKey(name))
            {
                int program = shaderPrograms[name];
                GL.UseProgram(program);
                boundProgram = program;
                return program;
            }
            else
            {
                LOG.e("Trying to bind non-existing shader "+name);
                return -1;
            }            
        }

        //Enable an attribute in the currently bound shader, returns the attribute location
        public static int EnableAttribute(String attributeName)
        {
            //Get the attribute location
            int location = GL.GetAttribLocation(boundProgram, attributeName);
            if (location == -1) LOG.e("Attribute "+attributeName+" not found!");

            //Enable the attribute
            GL.EnableVertexAttribArray(location);

            return location;
        }

        //Disable an attribute by location
        public static void DisableAttribute(int location)
        {
            //Enable the attribute
            GL.DisableVertexAttribArray(location);
        }

        //Find the location of a uniform
        public static int GetUniformLocation(string uniform)
        {
            int location = GL.GetUniformLocation(boundProgram, uniform);
            if (location == -1)
            {
                LOG.d("Failed to find uniform "+uniform);
            }
            return location;
        }

        //Set a float uniform for a shader program
        public static void SetUniform(string uniform, float value)
        {
            int location = GetUniformLocation(uniform);
            GL.Uniform1(location, value);
        }

        //Set a integer uniform for a shader program
        public static void SetUniform(string uniform, int value)
        {
            int location = GetUniformLocation(uniform);
            GL.Uniform1(location, value);
        }

        //Set a vec3 uniform for a shader program
        public static void SetUniform(string uniform, Vector3 value)
        {
            int location = GetUniformLocation(uniform);
            GL.Uniform3(location, value);
        }

        //Set a matrix uniform for a shader program
        public static void SetUniform(string uniform, Matrix4 value)
        {
            int location = GetUniformLocation(uniform);
            GL.UniformMatrix4(location, false, ref value);
        }
    }
}
