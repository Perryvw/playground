﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using Playground.Graphics.DDSReader;

namespace Playground.Graphics.Managers
{
    static class TextureManager
    {
        static private Dictionary<string, int> textures = new Dictionary<string, int>();

        public static void Init()
        {            
        }

        public static int loadTexture(string path)
        {
            path = "Resources\\Textures\\"+path;
            if (textures.ContainsKey(path))
            {
                return textures[path];
            }
            else
            {
                int texId = GL.GenTexture();
                GL.BindTexture(TextureTarget.Texture2D, texId);

                Bitmap image = null;
                if (!System.IO.File.Exists(path))
                {
                    return -1;
                }
                DDSImage ddsImg = new DDSImage(System.IO.File.ReadAllBytes(path));
                image = ddsImg.BitmapImage;
                ddsImg = null;

                BitmapData bmpData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);

                image.UnlockBits(bmpData);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
                GL.BindTexture(TextureTarget.Texture2D, 0);
                textures[path] = texId;
                return texId;
            }
        }
    }
}
