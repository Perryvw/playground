﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

namespace Playground.Graphics
{
    class Light
    {
        public Vector3 Position;
        public float Strength;
        public Vector3 Color;
    }
}
