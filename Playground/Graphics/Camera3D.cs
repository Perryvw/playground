﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

using Playground.Core;

namespace Playground.Graphics
{
    class Camera3D
    {
        public Vector3 Position;
        public Vector3 Target;
        public Matrix4 ProjectionMatrix;

        private float FoV = (float)Math.PI / 3f;
        private float AspectRatio = 16f / 9f;
        private float zNear = 0.1f;
        private float zFar = 100f;

        //Camera constructor with default values
        public Camera3D()
        {
            this.Position = new Vector3(0, -1, 0);
            this.Target = new Vector3(0, 0, 0);
            SetProjectionMatrix(FoV, AspectRatio, zNear, zFar);
        }

        //Camera constructor with specified field of view
        public Camera3D(float FoV)
        {
            this.Position = new Vector3(0, -1, 0);
            this.Target = new Vector3(0, 0, 0);
            this.FoV = FoV;
            SetProjectionMatrix(FoV, AspectRatio, zNear, zFar);
        }

        //Adjust the camera's projection matrix to the new aspect ratio of the screen
        public void Resize()
        {
            this.AspectRatio = (float)Game.ScreenWidth/(float)Game.ScreenHeight;
            SetProjectionMatrix(FoV, AspectRatio, zNear, zFar);
        }

        //Get the model-view-projection matrix
        public Matrix4 GetViewProjectionMatrix()
        {
            return this.GetViewMatrix() * this.ProjectionMatrix;
        }

        //Set the projection matrix for this camera
        public void SetProjectionMatrix(float FoV, float aspectRatio, float zNear, float zFar)
        {
            this.ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(FoV, aspectRatio, zNear, zFar);
        }

        //Get the model-view matrix for this camera
        public Matrix4 GetViewMatrix()
        {
            return Matrix4.LookAt(this.Position, this.Target, new Vector3(0, 0, 1));
        }
    }
}
