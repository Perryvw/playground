﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Playground.Core;
using Playground.World;
using Playground.Graphics.Managers;

namespace Playground.Graphics
{
    static class RenderEngine
    {
        private static Matrix4 ViewProjectionMatrix;
        private static Dictionary<String, uint[]> FrameBuffers;
        
        //Initialise the rendering engine
        public static void Init()
        {
            LOG.v("Initialising render engine...");
            
            //Initialise managers
            ShaderManager.Init();
            TextureManager.Init();
            
            //Load shaders
            ShaderManager.MakeProgram("Props.Basic");
            ShaderManager.MakeProgram("Terrain.Basic");
            ShaderManager.MakeProgram("Nature.Basic");
            ShaderManager.MakeProgram("Water.Pass1");

            //Initialise frame buffers
            FrameBuffers = new Dictionary<string, uint[]>();
            MakeFBO("Basic");

            LOG.v("Render engine initialised.");
        }

        //Render the gameworld
        public static void Render()
        {
            //Basic render pass
            BasicRenderPass();

            //clear the viewport before the final render pass
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            //Water render pass
            WaterRenderPass();
        }

        //Basic rendering pass, renders terrain without any water or lights
        private static void BasicRenderPass()
        {
            //Bind the frame buffer
            BindFBO("Basic");
            
            //Enable depth test
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.AlphaFunc(AlphaFunction.Greater, 0.1f);

            //Process the lighting data

            //Get the camera matrix
            ViewProjectionMatrix = GameWorld.Camera.GetViewProjectionMatrix();

            //Draw the world
            DrawTerrain();
            DrawNature();
            DrawProps();

            //Reset rendering state
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.AlphaTest);

            //Unbind the frame buffer
            UnbindFBO("Basic");
        }

        private static void WaterRenderPass()
        {
            //Enable depth test
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.AlphaTest);
            GL.AlphaFunc(AlphaFunction.Greater, 0.1f);

            //Process the lighting data

            //Get the camera matrix
            ViewProjectionMatrix = GameWorld.Camera.GetViewProjectionMatrix();

            //Draw the world
            DrawTerrain();
            DrawNature();
            DrawProps();

            //Draw bodies of water
            DrawWater();

            //Reset rendering state
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.AlphaTest);
        }

        //Draw the terrain
        private static void DrawTerrain()
        {
            //Bind the shader program
            int shaderProgram = ShaderManager.BindProgram("Terrain.Basic");

            //Set the view projection matrix in the shader
            ShaderManager.SetUniform("viewProjectionMatrix", ViewProjectionMatrix);
            GameWorld.Terrain.Draw();
        }

        //Draw nature (trees/foilage)
        private static void DrawNature()
        {
            //Bind the shader program
            int shaderProgram = ShaderManager.BindProgram("Nature.Basic");

            //Set the view projection matrix in the shader
            ShaderManager.SetUniform("viewProjectionMatrix", ViewProjectionMatrix);

            //Set the wind uniform
            ShaderManager.SetUniform("u_wind", GameWorld.Environment.Wind);

            GameWorld.DrawNature();
        }

        //Draw props in the world
        private static void DrawProps()
        {
            //Bind the shader program
            int shaderProgram = ShaderManager.BindProgram("Props.Basic");

            //Set the view projection matrix in the shader
            ShaderManager.SetUniform("viewProjectionMatrix", ViewProjectionMatrix);
            
            //Draw all props
            GameWorld.DrawProps();
        }

        //Draw all bodies of water
        private static void DrawWater()
        {
            //Bind the shader program
            int shaderProgram = ShaderManager.BindProgram("Water.Pass1");

            //Set the view projection matrix in the shader
            ShaderManager.SetUniform("viewProjectionMatrix", ViewProjectionMatrix);

            //pass the screen dimension to the shader
            ShaderManager.SetUniform("u_ScreenWidth", Game.ScreenWidth);
            ShaderManager.SetUniform("u_ScreenHeight", Game.ScreenHeight);

            //Bind the result of the previous render
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, FrameBuffers["Basic"][1]);
            ShaderManager.SetUniform("renderTexture1", 0);

            //Draw all props
            GameWorld.DrawWater();
        }

        //Make an FBO with identifier
        private static void MakeFBO(String name)
        {
            uint fboHandle;
            uint colorTex;
            uint depthTex;

           //Create depth texture
            GL.GenTextures(1, out depthTex);

            // Still required else TexImage2D will be applyed on the last bound texture
            GL.BindTexture(TextureTarget.Texture2D, depthTex);

            // generate null texture
            GL.TexImage2D(TextureTarget.Texture2D, 0, (PixelInternalFormat)All.DepthComponent32, Game.ScreenWidth, Game.ScreenHeight, 0,
            PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);

            // set filtering
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            //make the fbo texture
            GL.GenTextures(1, out colorTex);

            //bind texture
            GL.BindTexture(TextureTarget.Texture2D, colorTex);

            // generate null texture
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, Game.ScreenWidth, Game.ScreenHeight, 0,
            OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);

            //Set filtering
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            //Make the FBO
            GL.Ext.GenFramebuffers(1, out fboHandle);
            GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, fboHandle);

            //Bind the texture to the FBO
            GL.Ext.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, colorTex, 0);

            //Bind depth texture
            GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, TextureTarget.Texture2D, depthTex, 0);
            
            //Unbind the FBO for now
            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);

            FrameBuffers[name] = new uint[3]{fboHandle, colorTex, depthTex};
        }

        //Bind an FBO by indentifier
        private static void BindFBO(String name)
        {
            //Check if the frame buffer exists
            if (!FrameBuffers.ContainsKey(name))
            {
                LOG.e("Trying to bind framebuffer " + name + " - not found.");
            }

            //Bind the frame buffer
            GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, FrameBuffers[name][0]);

            //Set the viewport
            GL.PushAttrib(AttribMask.ViewportBit);
            GL.Viewport(0, 0, Game.ScreenWidth, Game.ScreenHeight);

            //clear the viewport
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        //Unbind an FBO by identifier
        private static void UnbindFBO(String name)
        {
            //Check if the frame buffer exists
            if (!FrameBuffers.ContainsKey(name))
            {
                LOG.e("Trying to unbind framebuffer " + name + " - not found.");
            }

            //Pop viewport
            GL.PopAttrib();
            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
        }
    }
}
