﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Playground.Datatypes;
using Playground.Graphics.Managers;

namespace Playground.Graphics
{
    /*
     * A 3D Model, consisting of a vertex buffer object (VBO), and an element buffer.
     */
    class AnimModel3D : Model3D
    {
        public AnimModel3D(int vertexBuffer, int elementBuffer, int numFaces, int texture)
            : base(vertexBuffer, elementBuffer, numFaces, texture)
        {
        }

        public override void Draw()
        {
            //Set texture uniform
            if (this.Texture != -1)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, this.Texture);
                ShaderManager.SetUniform("colorTex", 0);
            }
            
            //Set GL client state
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.NormalArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);

            //Enable attributes and get their locations
            int indicesAttribute = ShaderManager.EnableAttribute("a_Indices");
            int weightsAttribute = ShaderManager.EnableAttribute("a_Weights");

            //Bind the vertex buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, this.VertexBuffer);

            //Point to the vertices, normals and texture coordinates
            GL.VertexPointer(3, VertexPointerType.Float, AnimVertex.Size, AnimVertex.VertexOffset);
            GL.NormalPointer(NormalPointerType.Float, AnimVertex.Size, AnimVertex.NormalOffset);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, AnimVertex.Size, AnimVertex.TexCoordOffset);

            //Point to our custom attributes
            GL.VertexAttribPointer(indicesAttribute, 4, VertexAttribPointerType.Float, false, AnimVertex.Size, AnimVertex.IndicesOffset);
            GL.VertexAttribPointer(weightsAttribute, 4, VertexAttribPointerType.Float, false, AnimVertex.Size, AnimVertex.WeightOffset);

            //Bind the element buffer
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, this.ElementBuffer);

            //Draw the buffers
            GL.DrawElements(PrimitiveType.Triangles, NumFaces * 3, DrawElementsType.UnsignedInt, IntPtr.Zero);

            //Disable attributes
            ShaderManager.DisableAttribute(indicesAttribute);
            ShaderManager.DisableAttribute(weightsAttribute);

            //Revert GL client state
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.NormalArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }

        //Build a model3d from a list of vertices and faces
        public static AnimModel3D FromVertsFaces(AnimVertex[] vertices, int[] faces, int texture)
        {
            //Set up handles
            int vertexBuffer = -1;
            int elementBuffer = -1;

            //Generate the vertex buffer
            GL.GenBuffers(1, out vertexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuffer);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(AnimVertex.Size * vertices.Length), vertices, BufferUsageHint.StaticDraw);

            //Generate the element buffer
            GL.GenBuffers(1, out elementBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(int) * faces.Length), faces, BufferUsageHint.StaticDraw);

            //Unbind both buffers
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            //Return a new model with the loaded data
            return new AnimModel3D(vertexBuffer, elementBuffer, faces.Length / 3, texture);
        }
    }
}
