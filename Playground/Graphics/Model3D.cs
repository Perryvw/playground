﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Playground.Datatypes;
using Playground.Graphics.Managers;

namespace Playground.Graphics
{
    /*
     * A 3D Model, consisting of a vertex buffer object (VBO), and an element buffer.
     */
    class Model3D
    {
        protected int VertexBuffer;
        protected int ElementBuffer;
        protected int NumFaces;

        protected int Texture;

        public Model3D(int vertexBuffer, int elementBuffer, int numFaces, int texture)
        {
            this.VertexBuffer = vertexBuffer;
            this.ElementBuffer = elementBuffer;
            this.NumFaces = numFaces;
            this.Texture = texture;
        }

        //Set the texture for this model
        public void SetTexture(String textureName)
        {
            this.Texture = TextureManager.loadTexture(textureName);
        }

        //Draw this model
        public virtual void Draw()
        {
            //Set texture uniform
            if (this.Texture != -1)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, this.Texture);
                ShaderManager.SetUniform("colorTex", 0);
            }
            
            //Set GL client state
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.NormalArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);

            //Bind the vertex buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, this.VertexBuffer);

            //Point to the vertices, normals and texture coordinates
            GL.VertexPointer(3, VertexPointerType.Float, Vertex.Size, Vertex.VertexOffset);
            GL.NormalPointer(NormalPointerType.Float, Vertex.Size, Vertex.NormalOffset);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, Vertex.Size, Vertex.TexCoordOffset);

            //Bind the element buffer
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, this.ElementBuffer);

            //Draw the buffers
            GL.DrawElements(PrimitiveType.Triangles, NumFaces * 3, DrawElementsType.UnsignedInt, IntPtr.Zero);

            //Revert GL client state
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.NormalArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }

        public static void DrawCube(float size)
        {
            //draw sides
            GL.Begin(PrimitiveType.TriangleStrip);
            GL.Vertex3(-size / 2f, -size / 2f, 0);
            GL.Vertex3(-size / 2f, -size / 2f, size);
            GL.Vertex3(size / 2f, -size / 2f, 0);
            GL.Vertex3(size / 2f, -size / 2f, size);
            GL.Vertex3(size / 2f, size / 2f, 0);
            GL.Vertex3(size / 2f, size / 2f, size);
            GL.Vertex3(-size / 2f, size / 2f, 0);
            GL.Vertex3(-size / 2f, size / 2f, size);
            GL.Vertex3(-size / 2f, -size / 2f, 0);
            GL.Vertex3(-size / 2f, -size / 2f, size);
            GL.End();

            //draw top
            GL.Begin(PrimitiveType.TriangleStrip);
            GL.TexCoord2(0, 0);
            GL.Vertex3(-size / 2f, -size / 2f, size);
            GL.TexCoord2(0, 1);
            GL.Vertex3(-size / 2f, size / 2f, size);
            GL.TexCoord2(1, 0);
            GL.Vertex3(size / 2f, -size / 2f, size);
            GL.TexCoord2(1, 1);
            GL.Vertex3(size / 2f, size / 2f, size);
            GL.End();

            //draw bottom
            GL.Begin(PrimitiveType.TriangleStrip);
            GL.Vertex3(-size / 2f, -size / 2f, 0);
            GL.Vertex3(-size / 2f, size / 2f, 0);
            GL.Vertex3(size / 2f, -size / 2f, 0);
            GL.Vertex3(size / 2f, size / 2f, 0);
            GL.End();
        }

        //Build a model3d from a list of vertices and faces
        public static Model3D FromVertsFaces(Vertex[] vertices, int[] faces, int texture)
        {
            //Set up handles
            int vertexBuffer = -1;
            int elementBuffer = -1;

            //Generate the vertex buffer
            GL.GenBuffers(1, out vertexBuffer);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuffer);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(Vertex.Size * vertices.Length), vertices, BufferUsageHint.StaticDraw);

            //Generate the element buffer
            GL.GenBuffers(1, out elementBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(int) * faces.Length), faces, BufferUsageHint.StaticDraw);

            //Unbind both buffers
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            //Return a new model with the loaded data
            return new Model3D(vertexBuffer, elementBuffer, faces.Length / 3, texture);
        }
    }
}
