﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Playground
{
    /*
     * This class controls logging to the console through several levels
     * of output. The levels distinguished are: verbose, debug and error.
     */
    static class LOG
    {
        const bool verbose = true;
        const bool debug = true;
        const bool error = true;

        // Print a verbose message in the console
        public static void v(Object message)
        {
            if (verbose)
            {
                Console.ForegroundColor = ConsoleColor.White;
                WriteMessage("V", message.ToString());
            }
        }

        // Print a debug message in the console
        public static void d(Object message)
        {
            if (debug)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                WriteMessage("D", message.ToString());
            }
        }

        // Print an error message in the console
        public static void e(Object message)
        {
            if (error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                WriteMessage("E", message.ToString());
                throw (new Exception("An error has occured!"));
            }
        }

        // Write a message in the console with timestamp
        private static void WriteMessage(String level, String message)
        {
            //the message is formatted like *level*:[*timestamp*] *message*
            Console.WriteLine(level+":[" + DateTime.Now.ToString("HH:mm:ss") + "]: " + message);
        }
    }
}
