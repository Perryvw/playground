﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Input;

using Playground.Graphics;
using Playground.World;

namespace Playground.Core
{
    /*
     * Main class for the game.
     */
    class Game
    {
        public static GameWindow Window;
        public static int ScreenWidth = 1280;
        public static int ScreenHeight = 720;
        
        //Initialisation phase of the game
        public static void Init(GameWindow window)
        {
            LOG.v("Initialising Playground");

            Window = window;
        }

        //Called when the game window is loaded
        public static void OnLoad()
        {
            LOG.v("Window Loaded");

            //Set up windows
            Console.Title = "Playground Console";
            Window.Title = "Playground";
            Window.WindowBorder = WindowBorder.Hidden;

            //Initialise the render engine
            RenderEngine.Init();

            //Initialise the game world
            GameWorld.Init();
        }

        //Called when the game window is resized
        public static void OnResize(int WindowWidth, int windowHeight)
        {
            LOG.d("Window resized to: "+WindowWidth+"x"+windowHeight);

            ScreenWidth = WindowWidth;
            ScreenHeight = windowHeight;

            GameWorld.Camera.Resize();
        }

        //Update the game state
        public static void Update()
        {
            //Update current key state to handler
            InputHandler.GetState();

            //Update the gameworld
            GameWorld.Update();
        }

        //Render the game graphics
        public static void Render()
        {
            //Let the render engine render the game world
            RenderEngine.Render();
        }

        //Exit the application
        public static void Exit()
        {
            Window.Exit();
        }
    }
}
