﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK.Input;

namespace Playground.Core
{
    static class InputHandler
    {
        public static KeyboardState KeyState;
        
        public static void GetState()
        {
            if (Game.Window.Focused)
            {
                KeyState = Keyboard.GetState();
            }
        }
    }
}
